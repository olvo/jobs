# Cargonautes Test

Bonjour,

Nous souhaitons évaluer ton aisance avec Ruby et Ruby on Rails. Ce test technique servira de base de discussion.

Le but pour toi est de construire une petite application répondant à un besoin métier. Nous regarderons à la fois tes choix produit (quelles fonctionnalités ? quelles interfaces ?) et techniques (structure du code).

Il y a plusieurs étapes, chacune contenant des bonus. Tu n'es pas obligé de tout faire. Nous sommes plus intéressés par la qualité du travail produit que par la quantité.

Progresse à ton rythme et tiens-nous régulièrement informés.

## Résultat attendu

En tant que mécanicien d'une entreprise de cylo-logistique, je souhaite une application web pour gérer la flotte de vélos utilisés pour les livraisons.

Les vélos que nous possédons sont de modèles différents et n'ont pas été achetés en même temps. Ils sont identifiés par un numéro. Certains sont mécaniques et d'autres électriques.

Tous les jours, nos vélos parcourent entre 50 et 120 km. Ils sont donc sujets à des pannes. J'ai besoin de répertorier les pannes, qui sont de plusieurs types (crevaison, casse moteur, cassette usée...).

## Étapes

### 0. Crée ton application

Idéalement en utilisant Ruby on Rails.

### 1. Nos vélos

J'ai besoin de répertorier les vélos : en ajouter, modifier et supprimer. Je veux aussi pouvoir consulter la liste

Bonus :

- Joli design (mais n'y perd pas trop de temps non plus)
- Il y a déjà un exemple de vélo au démarrage de l'appli

### 2. Les pannes

Je veux pouvoir déclarer des pannes sur les vélos à chaque fois qu'elles surviennent.

Bonus :

- Historique des pannes
- Ajouter d'autres caractéristiques qui te paraissent pertinentes

### 3. Réparations

Je veux pouvoir indiquer qu'une panne a été réparée.

Bonus :

- Informations de réparation : je peux noter comment j'ai réparé et si j'ai utilisé du matériel

### 4. État des lieux

Je veux savoir à un instant t quels vélos sont disponibles pour rouler : ceux qui ne sont pas en panne.

Bonus :

- Statistiques des pannes : quelles sont les pannes les plus courantes ?
- Statistiques des vélos : quelles sont les vélos qui tombent le plus en panne ?
